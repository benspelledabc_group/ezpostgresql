// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


// https://jsonplaceholder.typicode.com/posts/

export const environment = {
    production: false,
    api: {
      base: "https://jsonplaceholder.typicode.com/",
      entries: "comments"
      // entries: "photos"
      // entries: "posts"
      // base: "http://localhost:3000/",
      // entries: "entries"
    },
    fastapi: {
      // base: "https://fastapi.spelledabc.org/api/",
      base: "https://192.168.1.123/api/",
      games_owned: "games_owned"
    },
    status_check: {
      base: "https://fastapi.spelledabc.org/status/check/",
      // base: "https://192.168.1.123/status/check/",
      status_endpoint: ""
    },
    vars: {
      API_URL: "https://192.168.1.123",
      // API_URL: "https://staging.spelledabc.org.",
      //API_URL: "https://spelledabc.org.",
      status_endpoint: ""
    }
  };

  /*
   * For easier debugging in development mode, you can import the following file
   * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
   *
   * This import should be commented out in production mode because it will have a negative impact
   * on performance if an error is thrown.
   */
  // import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
