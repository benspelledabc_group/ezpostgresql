export class SamplePostItem{
    postId: string;
    title: string;
    body: string;

    //assign vals from json to properties
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class SampleBlogPost {
    _id: string;
    guid: string;
    isActive: boolean;
    picture: string;
    age: number;
    name: string;
    gender: string;
    company: string;
    email: string;
    about: string;
    posts: SamplePostItem[];


    //assign vals from json to properties
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
