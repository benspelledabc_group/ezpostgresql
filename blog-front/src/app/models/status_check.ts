export class StatusResult {
    status: string;
    base_url: string;
    uptime_seconds: number;
    cpu_count: number;

    //assign vals from json to properties
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}


// {
//     "status": "online",
//     "base_url": "https://staging.spelledabc.org",
//     "status_check_url": "https://staging.spelledabc.org/status/check/",
//     "api_info_url": "https://staging.spelledabc.org/api/",
//     "swagger_url": "https://staging.spelledabc.org/api/docs",
//     "openapi_url": "https://staging.spelledabc.org/api/openapi.json",
//     "uptime_seconds": 53036.98,
//     "cpu_count": 3,
//     "memory": {
//     "free_gb": 0.8587455749511719,
//     "used_gb": 2.9360809326171875,
//     "total_gb": 3.7948265075683594
//     }
// }
