import { BlogUsers } from "./blog-users";

export class BlogPost {
    _id: string;
    author: BlogUsers;
    userId: string;
    that: string;
    title: string;
    body: string;


    //assign vals from json to properties
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
