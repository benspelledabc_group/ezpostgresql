// import { BlogUsers } from "./blog-users";

export class DiaryPost {
    _id: string;
    // author: BlogUsers;
    postdate: string;
    reviewed: boolean;
    approved: boolean;
    active: boolean;
    json_data: string;


    //assign vals from json to properties
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
