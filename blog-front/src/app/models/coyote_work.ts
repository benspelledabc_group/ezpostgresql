export class CoyotePost {
    sound_enabled: boolean;
    sound_file: string;
    weather_enabled: boolean;
    photo_path: string;

    //assign vals from json to properties
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}


// {
//     "id": 39561,
//     "json_data": [
//       {
//         "sound_enabled": true,
//         "sound_file": "./sounds/ICQ-OuOu.mp3"
//       },
//       {
//         "weather_enabled": false
//       },
//       {
//         "photo_path": "https://awsunicorns.s3.amazonaws.com/public/coyote_work/pics/2023/04/20/ce212af0eb944ccaa26e0346518d024e.jpg"
//       }
//     ],
//     "date": "2023-04-20T11:23:25.983384"
//   },
