export class PlayerSummary {
    appid: string;

    steamid: number;
    communityvisibilitystate: number;
    profilestate: number;
    personaname: string;
    profileurl: string;
    avatar: string;
    avatarmedium: string;
    avatarfull: string;
    avatarhash: string;
    lastlogoff: number;
    personastate: number;
    realname: string;
    primaryclanid: number;
    timecreated: number;
    personastateflags: number;
    loccountrycode: string;
    locstatecode: string;


    //assign vals from json to properties
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
