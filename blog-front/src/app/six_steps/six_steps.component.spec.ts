import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { SixStepsComponent } from "./six_steps.component";

describe("SixStepsComponent", () => {
  let component: SixStepsComponent;
  let fixture: ComponentFixture<SixStepsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SixStepsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SixStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
