import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { BlogPostService } from "../service/blog-post.service";
import { BlogPost } from "../models/blog-post";

@Component({
  selector: "app-six_steps",
  templateUrl: "./six_steps.component.html",
  styleUrls: ["./six_steps.component.css"]
})
export class SixStepsComponent implements OnInit {
  loading: boolean = true;
  posts: BlogPost[];

  constructor(private postService: BlogPostService, private router: Router) {}

  ngOnInit() {
    this.loading = false;
  }

}
