import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FastAPIService } from "../service/fastapi.service";
import { BlogPostService } from "../service/blog-post.service";
import { BlogPost } from "src/app/models/blog-post";
import { BlogUsers } from "../models/blog-users";

@Component({
  selector: "app-past-posts",
  templateUrl: "./past-posts.component.html",
  styleUrls: ["./past-posts.component.css"]
})
export class PastPostsComponent implements OnInit {
  loading: boolean = true;
  posts: BlogPost[];
  postAuthors: BlogUsers[];
  buffer: any; //i want to cry...

  constructor(private fastAPIService: FastAPIService, private router: Router) {}

  ngOnInit() {
    this.getPosts();
  }

  getPosts(){
    this.fastAPIService.SampleGetBlogPosts().subscribe((data)=>{
      this.buffer = data;
      this.posts = this.buffer;
      console.log(this.posts);
      this.loading = false;
    });
  }

  getPosts_lkg(){
    this.fastAPIService.PostsGetPosts().subscribe((data)=>{
      this.buffer = data;
      this.posts = this.buffer;
      console.log(this.posts);
      this.loading = false;
    });

  }

}
