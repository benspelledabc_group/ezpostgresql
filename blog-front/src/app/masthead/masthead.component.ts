import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-masthead',
  templateUrl: './masthead.component.html',
  styleUrls: ['./masthead.component.css'],
})
export class MastheadComponent implements OnInit {
  @Input()
  header: {
    title: "fancy title",
    subtitle: "cool subtitle"
  }
  // ptitle: "Maybe this works"

  // wtfork: "this is a test"

  constructor() {
    //console.log("masthead component");
   }

  ngOnInit() {
    //console.log(this.header);
  }

}
