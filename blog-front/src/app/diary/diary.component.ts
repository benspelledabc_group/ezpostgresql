import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { FastAPIService } from '../service/fastapi.service';
import { DiaryPost } from '../models/diary';
import { HttpClient } from "@angular/common/http";

// import { from, of } from 'rxjs';
// import { delay } from 'rxjs/internal/operators';
// import { concatMap } from 'rxjs/internal/operators';


@Component({
  selector: "app-diary",
  templateUrl: "./diary.component.html",
  styleUrls: ["./diary.component.css"]
})
export class DiaryComponent implements OnInit {
  myArray = [1, 2, 3, 4];
  desiredSimulatedDelay: number = 2;
  loading: boolean = true;
  diary_posts: any; //f this makes my soul cry...

  constructor(public fastAPIService: FastAPIService) {
    //do work
  }

  ngOnInit() {
    this.getDiaryPosts();
  }


  getDiaryPosts() {
    this.fastAPIService.DiaryGetApproved().subscribe((data)=>{
    // this.fastAPIService.DiaryGetPending().subscribe((data) => {
      console.log(data);
      this.diary_posts = data;
      this.loading = false;
    });
  }


}
