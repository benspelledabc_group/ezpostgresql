import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

// import { BlogPostService } from "../service/blog-post.service";
// import { BlogPost } from "../models/blog-post";

import { FastAPIService } from "../service/fastapi.service";
import { SteamGame } from "../models/steam-games";
import { PlayerSummary } from "../models/player_summary";

@Component({
  selector: "app-steam",
  templateUrl: "./steam.component.html",
  styleUrls: ["./steam.component.css"]
})
export class SteamComponent implements OnInit {
  loading: boolean = true;
  //posts: BlogPost[];
  gamesOwned: SteamGame[];
  gamesRecentlyPlayed: SteamGame[];
  friend_summaries: PlayerSummary[]; //i want to cry...
  buffer: any; //i want to cry...

  constructor(private fastAPIService: FastAPIService, private router: Router) {}

  ngOnInit() {
    // this.delay(1000).then(any=>{
    //   this.getGames();
    //   this.getRecentGames();
    //   this.getFriendSummaries();
    // });
    this.getFriendSummaries();
    this.getRecentlyPlayed();
    this.getGamesOwned();
  }

  getFriendSummaries(){
    this.fastAPIService.SteamGetFriendSummaries().subscribe((data)=>{
      this.buffer = data;
      this.friend_summaries = this.buffer.result.response.players;
      this.friend_summaries.sort((a, b) => (a.personaname > b.personaname) ? 1 : -1)

      console.log(this.friend_summaries);



      this.loading = false;
    });
  }

  getRecentlyPlayed(){
    this.fastAPIService.SteamGetRecentlyPlayed().subscribe((data)=>{
      this.buffer = data;
      // console.log(data);
      this.gamesRecentlyPlayed = this.buffer.result.response.games;
      // this.friend_summaries.sort((a, b) => (a.personaname > b.personaname) ? 1 : -1)
      // console.log(this.friend_summaries);
      this.loading = false;
    });
  }

  getGamesOwned(){
    this.fastAPIService.SteamGetGamesOwned().subscribe((data)=>{
      this.buffer = data;
      // console.log(data);
      this.gamesOwned = this.buffer.result.response.games;
      this.gamesOwned.sort((a, b) => (a.playtime_forever < b.playtime_forever) ? 1 : -1)
      this.loading = false;
    });
  }


  // private getFriendSummaries(): void {
  //   this.loading = true;
  //   this.fastAPIService.Get("friendsummaries").subscribe(summaries => {
  //     this.friend_summaries = summaries.result.response.players;
  //     // i'm not sure this sort is working. add it to the TODO list.
  //     //this.friend_summaries.sort((a,b) => (a.lastlogoff < b.lastlogoff) ? 1 : -1)
  //     this.friend_summaries.sort((a,b) => (a.personaname > b.personaname) ? 1 : -1)
  //     console.log(this.friend_summaries)
  //   });
  //   this.loading = false;
  // }

  // private getGames(): void {
  //   this.loading = true;
  //   this.fastAPIService.Get("games_owned").subscribe(games => {
  //     this.games = games.result.response.games;
  //     //sort by playtime
  //     this.games.sort((a,b) => (a.playtime_forever < b.playtime_forever) ? 1 : -1)

  //     // console.log(this.games);
  //     this.loading = false;
  //   });
  // }


  // private getRecentGames(): void {
  //   this.loading = true;
  //   this.fastAPIService.Get("recently_played").subscribe(games => {
  //     this.gamesRecentlyPlayed = games.result.response.games;
  //     //sort by playtime
  //     //this.games.sort((a,b) => (a.playtime_forever < b.playtime_forever) ? 1 : -1)

  //     // console.log(this.gamesRecentlyPlayed);
  //     this.loading = false;
  //   });
  // }

  // private delay(ms: number) {
  //   return new Promise( resolve => setTimeout(resolve, ms) );
  // }

}
