import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { CoyoteWorkComponent } from "./coyote_work.component";

describe("CoyoteWorkComponent", () => {
  let component: CoyoteWorkComponent;
  let fixture: ComponentFixture<CoyoteWorkComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CoyoteWorkComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoyoteWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
