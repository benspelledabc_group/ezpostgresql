import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { BlogPostService } from "../service/blog-post.service";
import { BlogPost } from "../models/blog-post";

import { FastAPIService } from '../service/fastapi.service';
import { CoyotePost } from '../models/coyote_work';
import { HttpClient } from "@angular/common/http";

// import { from, of } from 'rxjs';
// import { delay } from 'rxjs/internal/operators';
// import { concatMap } from 'rxjs/internal/operators';


@Component({
  selector: "app-coyotework",
  templateUrl: "./coyote_work.component.html",
  styleUrls: ["./coyote_work.component.css"]
})
export class CoyoteWorkComponent implements OnInit {
  myArray = [1, 2, 3, 4];
  desiredSimulatedDelay: number = 2;
  loading: boolean = true;
  coyote_posts: any; //f this makes my soul cry...

  constructor(public fastAPIService: FastAPIService) {
    //do work
  }

  ngOnInit() {
    //do work
    //this.simulate_load();
    this.getCoyotePosts();
  }

  getCoyotePosts(){
    this.fastAPIService.GetCoyoteWorkLastX(5).subscribe((data)=>{
      console.log(data);
      this.coyote_posts = data;
      this.loading = false;
    });
  }

}
