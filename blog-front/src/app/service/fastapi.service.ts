import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Bug } from './bug';
import { StatusResult } from '../models/status_check';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { PlayerSummary } from "../models/player_summary";
import { SteamGame } from "../models/steam-games";
import { BlogPost } from "../models/blog-post";
import { BlogUsers } from "../models/blog-users";
import { SampleBlogPost } from "../models/sample-blog-post";
import { DiaryPost } from "../models/diary";

import { environment } from "../../environments/environment";
const API_URL = environment.vars.API_URL;


@Injectable({
  providedIn: 'root',
})
export class FastAPIService {
  // Base url
  // wtf = env.get('DATABASE_URL')

  // baseurl = 'https://fastapi.spelledabc.org';
  baseurl = API_URL;
  //baseurl = 'https://spelledabc.org';
  // baseurl = 'https://spelledabc.org_fastapi';
  //baseurl = 'https://192.168.1.123';
  placeHolderurl = 'https://jsonplaceholder.typicode.com';

  constructor(private http: HttpClient){
    // console.log(this.wtf);
  }
  
  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  // GET
  GetStatus(): Observable<StatusResult> {
    return this.http
      .get<StatusResult>(this.baseurl + '/status/check/')
      .pipe(retry(1), catchError(this.errorHandl));
  }

  GetCoyoteWorkLastX(lastX): Observable<StatusResult> {
    return this.http
      .get<StatusResult>(this.baseurl + '/api/sensor_posts/id/desc/latest/{count}?latest_records=' + lastX)
      .pipe(retry(1), catchError(this.errorHandl));
  }

  // STEAM STUFF
  // get friend summeries
  SteamGetFriendSummaries(): Observable<PlayerSummary> {
    return this.http
      .get<PlayerSummary>(this.baseurl + '/api/friendsummaries')
      .pipe(retry(1), catchError(this.errorHandl));
  }

  SteamGetRecentlyPlayed(): Observable<SteamGame> {
    return this.http
      .get<SteamGame>(this.baseurl + '/api/recently_played')
      .pipe(retry(1), catchError(this.errorHandl));
  }

  SteamGetGamesOwned(): Observable<SteamGame> {
    return this.http
      .get<SteamGame>(this.baseurl + '/api/games_owned')
      .pipe(retry(1), catchError(this.errorHandl));
  }


  SampleGetBlogPosts(): Observable<SampleBlogPost> {
    return this.http
      .get<SampleBlogPost>(this.baseurl + '/api/sample/blogposts')
      .pipe(retry(1), catchError(this.errorHandl));
  }






  // https://jsonplaceholder.typicode.com/posts/
  // https://jsonplaceholder.typicode.com/users
  // https://jsonplaceholder.typicode.com/users/2

  PostsGetPosts(): Observable<BlogPost> {
    return this.http
      .get<BlogPost>(this.placeHolderurl + '/posts/')
      .pipe(retry(1), catchError(this.errorHandl));
  }

  PostsUsers(): Observable<BlogUsers> {
    return this.http
      .get<BlogUsers>(this.placeHolderurl + '/users/')
      .pipe(retry(1), catchError(this.errorHandl));
  }

  PostsUserID(userID): Observable<BlogUsers> {
    return this.http
      .get<BlogUsers>(this.placeHolderurl + '/users/' + userID)
      .pipe(retry(1), catchError(this.errorHandl));
  }




  // diary: getApproved, getNotApproved, getPendingApproval, getAll
  DiaryGetApproved(): Observable<DiaryPost> {
    return this.http
      .get<DiaryPost>(this.baseurl + '/api/db/diary/posts/Approved/')
      .pipe(retry(1), catchError(this.errorHandl));
  }

  DiaryGetNotApproved(): Observable<DiaryPost> {
    return this.http
      .get<DiaryPost>(this.baseurl + '/api/db/diary/posts/Not%20Approved/')
      .pipe(retry(1), catchError(this.errorHandl));
  }

  DiaryGetPending(): Observable<DiaryPost> {
    return this.http
      .get<DiaryPost>(this.baseurl + '/api/db/diary/posts/Pending%20Approval/')
      .pipe(retry(1), catchError(this.errorHandl));
  }

  DiaryGetAll(): Observable<DiaryPost> {
    return this.http
      .get<DiaryPost>(this.baseurl + '/api/db/diary/posts/All/')
      .pipe(retry(1), catchError(this.errorHandl));
  }











  // Error handling
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(() => {
      return errorMessage;
    });
  }
}
