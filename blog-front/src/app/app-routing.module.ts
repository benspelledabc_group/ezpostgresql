import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { QRLinkComponent } from "src/app/qr-link/qr-link.component";
import { DiaryComponent } from "src/app/diary/diary.component";
import { HomeComponent } from "src/app/home/home.component";
import { SixStepsComponent } from "src/app/six_steps/six_steps.component";
import { CoyoteWorkComponent } from "src/app/coyote_work/coyote_work.component";
import { AboutComponent } from "src/app/about/about.component";
import { ContactComponent } from "src/app/contact/contact.component";
import { PastPostsComponent } from "src/app/past-posts/past-posts.component";
import { AuthorPostComponent } from "src/app/author-post/author-post.component";
import { ViewPostComponent } from "src/app/view-post/view-post.component";
import { SteamComponent } from "src/app/steam/steam.component";


// https://www.w3schools.com/charsets/ref_emoji.asp
const routes: Routes = [
  // { path: "home", component: HomeComponent, data : {title:'✓🗽Home', extra:'wtfsauce'}},
  { path: "home", component: HomeComponent, data : {title:'Home', extra:'wtfsauce'}},
  // { path: "diary", component: DiaryComponent, data : {title:'✓🐒Diary'} },
  { path: "diary", component: DiaryComponent, data : {title:'Diary'} },
  { path: "six_steps", component: SixStepsComponent, data : {title:'Six Steps'} },
  // { path: "coyote_work", component: CoyoteWorkComponent, data : {title:'✓💀Coyote Work'}  },
  { path: "coyote_work", component: CoyoteWorkComponent, data : {title:'Coyote Work'}  },
  { path: "about", component: AboutComponent, data : {title:'About'}   },
  // { path: "contact", component: ContactComponent, data : {title:'💩Contact'}   },
  { path: "contact", component: ContactComponent, data : {title:'Contact'}   },
  { path: "posts", component: PastPostsComponent, data : {title:'Blog Posts'}   },
  { path: "post/:id", component: ViewPostComponent, data : {title:'Post#:'}   },
  { path: "author-post", component: AuthorPostComponent, data : {title:'Author Post'}   },
  // { path: "steam", component: SteamComponent, data : {title:'🔞Steam Stuff'}   },
  { path: "steam", component: SteamComponent, data : {title:'Steam Stuff'}   },

  { path: "qr-link", component: QRLinkComponent, data : {title:'QR Link'}   },
  { path: "qr", component: QRLinkComponent, data : {title:'QR Link'}   },
  { path: "**", redirectTo: "/home", data : {title:'Home', extra:'wtfsauce', note:"'data' is in app-routing.module.ts"}},
];

@NgModule({
  imports: [
    // RouterModule.forRoot(routes, {}),
    RouterModule.forRoot(routes,{scrollPositionRestoration:'enabled'})
  ],

  exports: [RouterModule]
})
export class AppRoutingModule {}
