import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-qr-link",
  templateUrl: "./qr-link.component.html",
  styleUrls: ["./qr-link.component.css"]
})
export class QRLinkComponent implements OnInit {
  loading: boolean = true;

  constructor() {}

  ngOnInit() {
    this.loading = false;
  }
}
