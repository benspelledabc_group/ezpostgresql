import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { QRLinkComponent } from "./qr-link.component";

describe("QRLinkComponent", () => {
  let component: QRLinkComponent;
  let fixture: ComponentFixture<QRLinkComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [QRLinkComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QRLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
