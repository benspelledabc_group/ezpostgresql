import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { FastAPIService } from "../service/fastapi.service";
import { PlayerSummary } from "../models/player_summary";


@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"]
})
export class NavbarComponent implements OnInit {
  loading: boolean = true;
  players: PlayerSummary[];
  ben_summary: PlayerSummary;

  constructor(private fastAPIService: FastAPIService, private router: Router) { }

  ngOnInit() {
    //this.getGames();
  }

  // private getGames(): void {
  //   this.fastAPIService.Get("player_summary").subscribe(players => {
  //     // this.players = players.result.response.games;
  //     this.players = players.result.response.players;

  //     //console.log(this.players);
  //     this.ben_summary = this.players[0];
  //     // console.log(this.ben_summary);
  //     this.loading = false;
  //   });
  // }

}
