import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { faDiscord, faDAndD, faDocker } from '@fortawesome/free-brands-svg-icons';

// import {  } from "../service/status_check.service";
import { FastAPIService } from '../service/fastapi.service';
import { StatusResult } from "../models/status_check";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.css"]
})
export class FooterComponent implements OnInit {
  year: string;
  loading: boolean = true;
  status_result: StatusResult; //this should be StatusResult not 'any'... fix it TODO
  faDiscord = faDiscord;
  faDAndD = faDAndD;
  faDocker = faDocker;

  constructor(public fastAPIService: FastAPIService) {
    //do work
  }

  ngOnInit() {
    this.year = new Date().getFullYear().toString();
    this.getStatus();
    this.loading = false;
  }

  getStatus(){
    this.fastAPIService.GetStatus().subscribe((data)=>{
      // console.log(data);
      this.status_result = data;
    })
  }

}
