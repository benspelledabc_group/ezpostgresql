import { BrowserModule, Title } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { HttpClientModule  } from "@angular/common/http";
// import { Http, HttpModule } from "@angular/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { QRLinkComponent } from "./qr-link/qr-link.component";
import { FastAPIService } from './service/fastapi.service';

import { AppComponent } from "./app.component";
import { DiaryComponent } from "./diary/diary.component";

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { HomeComponent } from "./home/home.component";
import { SixStepsComponent } from "./six_steps/six_steps.component";
import { CoyoteWorkComponent } from "./coyote_work/coyote_work.component";
import { AboutComponent } from "./about/about.component";
import { SteamComponent } from "./steam/steam.component";
import { ContactComponent } from "./contact/contact.component";
import { PastPostsComponent } from "./past-posts/past-posts.component";
import { ViewPostComponent } from "./view-post/view-post.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { FooterComponent } from "./footer/footer.component";
import { MastheadComponent } from "./masthead/masthead.component";
import { PreviewPostComponent } from "./preview-post/preview-post.component";
import { AuthorPostComponent } from "./author-post/author-post.component";
import { LoadingComponent } from "./loading/loading.component";

@NgModule({
  declarations: [
    AppComponent,
    QRLinkComponent,
    DiaryComponent,
    HomeComponent,
    SixStepsComponent,
    CoyoteWorkComponent,
    AboutComponent,
    SteamComponent,
    ContactComponent,
    PastPostsComponent,
    ViewPostComponent,
    NavbarComponent,
    FooterComponent,
    MastheadComponent,
    PreviewPostComponent,
    AuthorPostComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [FastAPIService, Title],
  bootstrap: [AppComponent]
})
export class AppModule {}
