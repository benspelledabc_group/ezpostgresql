#!/bin/bash

ng build

rm -rf /workspace/letsencrypt_komplex/nginx/site
mkdir /workspace/letsencrypt_komplex/nginx/site

#echo staging.spelledabc.org
cp -R /home/benspelledabc/blog3/blog-front/dist/angular-blog-clean/* /workspace/letsencrypt_komplex/nginx/site/
docker-compose -f /workspace/letsencrypt_komplex/docker-compose.yml down && docker-compose -f /workspace/letsencrypt_komplex/docker-compose.yml up -d
