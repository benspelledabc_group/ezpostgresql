# Let's Encrypt (with extras)
Lets make a simpleish website that does cert issue/renewal the easy way.

## What's included?
1. Let's Encrypt cert create/renewal on container startup.
2. NGINX proxy to handle stuff.

## TODO
1. i'm finding things to fix in the code, but marking them as TODO for later.
