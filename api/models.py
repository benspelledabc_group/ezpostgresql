from enum import Enum

class Gender_Enum(str, Enum):
    male = "male"
    female = "female"
    both = "both"

class DiaryApproval_Enum(str, Enum):
    all = "All"
    approved = "Approved"
    not_approved = "Not Approved"
    pending = "Pending Approval"

# class GlobalApproval_Enum(str, Enum):
#     all = "All"
#     approved = "Approved"
#     not_approved = "Not Approved"
#     pending = "Pending Approval"
