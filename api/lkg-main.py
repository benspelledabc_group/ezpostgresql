# app/main.py
import os
from enum import Enum
from typing import Optional
from fastapi import FastAPI, Query
from fastapi.middleware.cors import CORSMiddleware
from api.db import database, User, Sensor_Posts, Diary_Posts
from api.models import Gender_Enum, DiaryApproval_Enum
from dotenv import load_dotenv
from uptime import uptime

import jwt
import os
import pwd
import requests
import json


app = FastAPI(title="Data Simplified",
              description="Data is the backbone to a digital wonderland.",
              version="x.9.11",
              docs_url='/api/docs',
              redoc_url='/api/redoc',
              openapi_url='/api/openapi.json',
)


# #######################
# load env keys
# #######################
load_dotenv()
baseurl=os.getenv('API_BASEURL')
status_check_url = "https://{0}/status/check/".format(baseurl)
api_baseurl="{0}/api".format(baseurl)
pguser=os.getenv('PG_USER')
pgpass=os.getenv('PG_PASS')
pghost=os.getenv('PG_HOST')
pgport=os.getenv('PG_PORT')
pgdb=os.getenv('PG_DB')
JWT_SECRET=os.getenv('JWT_SECRET')

user_key=os.getenv('USER_KEY')
steam_id=os.getenv('STEAM_ID')
default_app_id=os.getenv('DEFAULT_APP_ID')

# simulation only
#api_baseurl=default_app_id

json_folder="{0}/api/json".format(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

origins = [
    # Need to make the COREs a bit easier to use via .env
    "*",
    "http://localhost",
    "http://localhost:4200",
    "http://localhost:4201",
    "http://localhost:8080",
    "https://spelledabc.org",
    "https://ben.spelledabc.org",
    "https://staging.spelledabc.org",
    "https://db.spelledabc.org",
    "https://svc1.spelledabc.org",
    "https://svc2.spelledabc.org",
    "https://svc3.spelledabc.org",
    "https://srs.spelledabc.org",
    "https://coyote-work.spelledabc.org",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

def memory():
    """
    Get node total memory and memory usage
    """
    with open('/proc/meminfo', 'r') as mem:
        ret = {}
        ret_mb = {}
        tmp = 0
        for i in mem:
            sline = i.split()
            if str(sline[0]) == 'MemTotal:':
                ret['total'] = int(sline[1])
            elif str(sline[0]) in ('MemFree:', 'Buffers:', 'Cached:'):
                tmp += int(sline[1])
        ret['free'] = tmp
        ret['used'] = int(ret['total']) - int(ret['free'])

        ret_mb['free_gb'] = (ret['free'] /1024) / 1024
        ret_mb['used_gb'] = (ret['used'] /1024) / 1024
        ret_mb['total_gb'] = (ret['total'] /1024) / 1024

    return ret_mb


@app.get("/api/sensor_posts/", tags=["Coyote Work"])
async def all_sensor_posts():
    return await Sensor_Posts.objects.all()


@app.get("/api/sensor_posts/id/desc", tags=["Coyote Work"])
async def sensor_posts_by_id_desc():
    blob = await Sensor_Posts.objects.all()
    rval = list(reversed(blob))

    return rval


@app.get("/api/sensor_posts/id/desc/latest/", tags=["Coyote Work"])
async def sensor_posts_by_id_desc_limit_1():
    blob = await Sensor_Posts.objects.all()

    return blob[-1]


@app.get("/api/sensor_posts/id/desc/latest/{count}", tags=["Coyote Work"])
async def sensor_posts_by_id_desc_limit_x(latest_records: int):
    blob = await Sensor_Posts.objects.all()
    tmp = list(reversed(blob))

    rval = []
    for x in range(latest_records):
        rval.append(tmp[x])

    return rval


@app.get("/api/db/diary/posts/{diary_approval_enum}", tags=["Diary Posts"])
async def diary_posts_by_approval(diary_approval_enum: Optional[DiaryApproval_Enum] = DiaryApproval_Enum.approved):
    if diary_approval_enum is DiaryApproval_Enum.all:
        blob = await Diary_Posts.objects.all()
    elif diary_approval_enum is DiaryApproval_Enum.approved:
        blob = await Diary_Posts.objects.filter(approved=True).all()
    elif diary_approval_enum is DiaryApproval_Enum.not_approved:
        blob = await Diary_Posts.objects.filter(approved=False).all()
    elif diary_approval_enum is DiaryApproval_Enum.pending:
        blob = await Diary_Posts.objects.filter(approved__isnull = True).all()

    tmp = list(reversed(blob))
    rval = tmp

    return rval


@app.get("/api/db/diary/posts/{diary_approval_enum}/{max_count}", tags=["Diary Posts"])
async def diary_posts_by_approval_count(max_count : int, diary_approval_enum: Optional[DiaryApproval_Enum] = DiaryApproval_Enum.approved):
    if diary_approval_enum is DiaryApproval_Enum.all:
        blob = await Diary_Posts.objects.all()
    elif diary_approval_enum is DiaryApproval_Enum.approved:
        blob = await Diary_Posts.objects.filter(approved=True).all()
    elif diary_approval_enum is DiaryApproval_Enum.not_approved:
        blob = await Diary_Posts.objects.filter(approved=False).all()
    elif diary_approval_enum is DiaryApproval_Enum.pending:
        blob = await Diary_Posts.objects.filter(approved__isnull = True).all()

    tmp = list(reversed(blob))

    rval = []
    for x in range(max_count):
        try:
            rval.append(tmp[x])
        except Exception as ex:
            break;

    return rval


@app.get("/api/user/random/female", tags=["RandomUser"])
async def user_random_gender_female():
    url = f"https://randomuser.me/api/?gender=female"

    rval = {
        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {
            "status": "random_female",
            "result": requests.get(url).json()
        }
    except:
        rval = {

            "status": "error",
            "message": "things went sideways quick like, but we caught it"
        }

    return rval


@app.get("/api/user/random/male", tags=["RandomUser"])
async def user_random_gender_male():
    url = f"https://randomuser.me/api/?gender=male"

    return requests.get(url).json()


@app.get("/api/user/random/{count}", tags=["RandomUser"])
async def user_random_x(count : int):
    url = f"https://randomuser.me/api/?results={count}"

    return requests.get(url).json()


# default gender choice of 'both' isn't working right...
@app.get("/api/user/random/{count}/{gender}", tags=["RandomUser"])
async def user_random_x_gender(count : int, gender: Optional[Gender_Enum] = Gender_Enum.both):
    if gender is Gender_Enum.female:
        url = f"https://randomuser.me/api/?results={count}&gender=female"
    elif gender is Gender_Enum.male:
        url = f"https://randomuser.me/api/?results={count}&gender=male"
    elif gender is Gender_Enum.both:
        url = f"https://randomuser.me/api/?results={count}"


    return requests.get(url).json()


@app.get("/api/reddit/", tags=["Reddit"])
async def reddit():
    subreddit = 'TrendingReddits'
    limit = 6
    timeframe = 'day' #hour, day, week, month, year, all
    listing = 'top' # controversial, best, hot, new, random, rising, top

    rval = {
        "prefetch": "stuff did not load yet"
    }

    try:
        base_url = f'https://www.reddit.com/r/{subreddit}/{listing}.json?limit={limit}&t={timeframe}'
        request = requests.get(base_url, headers = {'User-agent': 'SpelledABC.org'})
        return request.json()
    except Exception as ex:
        rval = {"exception": "{0}".format(ex)}

    return rval


@app.get("/api/reddit/{subreddit}", tags=["Reddit"])
async def reddit(subreddit):
    from datetime import datetime
    myobj = datetime.now()
    cHour = myobj.hour

    limit = 100
    timeframe = 'week' #hour, day, week, month, year, all

    # it doenst like: random
    listing = 'best' # controversial, best, hot, new, random, rising, top

    rval = {
        "prefetch": "stuff did not load yet"
    }

    try:
        base_url = f'https://www.reddit.com/r/{subreddit}/{listing}.json?limit={limit}&t={timeframe}'
        request = requests.get(base_url, headers = {'User-agent': 'SpelledABC.org2'})
        #return request.json()
        r = request.json()

        posts = []
        for post in r['data']['children']:
            x = post['data']['title']
            posts.append(x)

        items = []
        rval = {
            "status": "done",
            "results": "",
        }
        for post in r['data']['children']:
            items.append({'title':post['data']['title'],'url':post['data']['url'],'score':post['data']['score'],'comments':post['data']['num_comments'],'is_video':post['data']['is_video'],'over_18':post['data']['over_18']})

        rval = {
            "status": "done",
            "results": items
        }

        #return myDict
        return rval

    except Exception as ex:
        print(ex)
        rval = {"exception": "we hit our data rate threshold"}

    return rval


@app.get('/api/employeelist/', tags=["Static File Test"])
async def employeelist():
    rval = {
        "endpoint_found": True,
    }

    with open(f'{json_folder}/employeelist.json', mode='r') as empList:
        content = empList.read()
        #rvalAsFile = empList.readlines()

    return json.loads(content)


@app.get('/api/sample/blogposts/', tags=["Static File Test"])
async def sampleblogposts():
    rval = {
        "endpoint_found": True,
    }

    with open(f'{json_folder}/sampleblogposts.json', mode='r') as empList:
        content = empList.read()
        #rvalAsFile = empList.readlines()

    return json.loads(content)


@app.get("/api/status/check", tags=["Status Tools"])
async def status_check():
        # old code use to return all users in the database
        # return await User.objects.all()
    rval = {
        "status": "online",
        "base_url": "https://{0}".format(baseurl),
        "status_check_url": status_check_url,
        "api_info_url": "https://{0}/api/".format(baseurl),
        "swagger_url": "https://{0}/api/docs".format(baseurl),
        "openapi_url": "https://{0}/api/openapi.json".format(baseurl),
        "uptime_seconds": uptime(),
        "cpu_count": os.cpu_count(),
        "memory": memory(),
    }
    return rval


@app.on_event("startup")
async def startup():
    if not database.is_connected:
        await database.connect()
    # create a dummy entry
    await User.objects.get_or_create(email="benspelledabc@gmail.com")

@app.on_event("shutdown")
async def shutdown():
    if database.is_connected:
        await database.disconnect()


@app.get("/api/db/read_test/", tags=["Status Tools"])
async def check_connection():
    return await User.objects.all()


# #########################################################################################
#
# endpoints from SteamApi: https://developer.valvesoftware.com/wiki/Steam_Web_API#JSON
# first the general endpoint default, then an override by id (where applciable)
#
# #########################################################################################
@app.get("/api/news_for_app/", tags=["Steam Powered"])
async def news_for_app():
    url = "http://api.steampowered.com/ISteamNews/GetNewsForApp/v0002/?appid={0}&count=3&maxlength=300&format=json".format(default_app_id)

    rval = {

        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {

            "status": "news_for_app(518790)",
            "result": requests.get(url).json()
        }
    except:
        rval = {

            "status": "error",
            "message": "things went sideways quick like, but we caught it"
        }

    return rval


@app.get("/api/news_for_app/{appid}", tags=["Steam Powered"])
async def news_for_app(appid, ):
    # http://localhost:8000/news_for_app/63380
    url = "http://api.steampowered.com/ISteamNews/GetNewsForApp/v0002/?appid={}&count=3&maxlength=300&format=json".format(appid)

    rval = {

        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {

            "status": "news_for_app",
            "result": requests.get(url).json()
        }
    except:
        rval = {

            "status": "error",
            "message": "things went sideways quick like, but we caught it"
        }

    return rval


@app.get("/api/friendsummaries", tags=["Steam Powered"])
async def friendsummaries():
    rval = {"status": "fucked up","status_code": 16947}

    try:
        objFriend = await friends()

        steam_id_list = "76561198041026797"
        for item in objFriend['result']['friendslist']['friends']:
            steam_id_list += f",{item['steamid']}"

        url = f"https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v2?key={user_key}&steamids={steam_id_list}"

        rval = {"status": "fucked up","status_code": 16947, "message": "we got the friend id list but failed to get the properties of said items."}

        rval = {
            "endpoint": "/api/friendsummaries",
            "result": requests.get(url).json()
        }
    except Exception as ex:
        rval = {
                "status": "fucked up but caught",
                "status_code": 16947,
                "message": f"{ex}"
            }

    return rval


@app.get("/api/achievement_percent_for_app", tags=["Steam Powered"])
async def achievement_percent_for_app():
    url = "http://api.steampowered.com/ISteamUserStats/GetGlobalAchievementPercentagesForApp/v0002/?gameid={}&format=json".format(default_app_id)

    rval = {

        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {

            "status": "achievement_percent_for_app(1144200)",
            "result": requests.get(url).json()
        }
    except:
        rval = {

            "status": "error",
            "message": "things went sideways quick like, but we caught it"
        }

    return rval


@app.get("/api/achievement_percent_for_app/{appid}", tags=["Steam Powered"])
async def achievement_percent_for_app(appid, ):
    # http://localhost:8000/news_for_app/63380
    url = "http://api.steampowered.com/ISteamUserStats/GetGlobalAchievementPercentagesForApp/v0002/?gameid={}&format=json".format(appid)

    rval = {

        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {

            "status": "achievement_percent_for_app",
            "result": requests.get(url).json()
        }
    except:
        rval = {

            "status": "error",
            "message": "things went sideways quick like, but we caught it"
        }

    return rval


@app.get("/api/player_summary", tags=["Steam Powered"])
async def player_summary():
    url = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key={}&steamids={}&format=json".format(user_key, steam_id)

    rval = {

        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {

            "status": "player_summary",
            "result": requests.get(url).json()
        }
    except:
        rval = {

            "status": "error",
            "message": "things went sideways quick like, but we caught it"
        }

    return rval


@app.get("/api/friends", tags=["Steam Powered"])
async def friends():
    url = "http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key={}&steamid={}&relationship=friend".format(user_key, steam_id)

    rval = {

        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {

            "status": "friends",
            "result": requests.get(url).json()
        }
    except:
        rval = {

            "status": "error",
            "message": "things went sideways quick like, but we caught it"
        }

    return rval


@app.get("/api/friend/{id}", tags=["Steam Powered"])
async def friend(id):
    url = f"http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key={user_key}&steamids={id}&format=json"

    rval = {

        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {

            "status": "friends",
            "result": requests.get(url).json()
        }
    except Exception as ex:
        rval = {
            "status": "error",
            "message": "things went sideways quick like, but we caught it"
        }
        print(ex)
    return rval


@app.get("/api/achievements_by_id/", tags=["Steam Powered"])
async def achievements_by_id():
    url = "http://api.steampowered.com/ISteamUserStats/GetPlayerAchievements/v0001/?appid={}&key={}&steamid={}".format(default_app_id, user_key, steam_id)

    rval = {

        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {

            "status": "achievements_by_id",
            "result": requests.get(url).json()
        }
    except:
        rval = {

            "status": "error",
            "message": "Player has this game but... shit got fucked up."
        }

    return rval


@app.get("/api/achievements_by_id/{appid}", tags=["Steam Powered"])
async def achievements_by_id(appid, ):
    url = "http://api.steampowered.com/ISteamUserStats/GetPlayerAchievements/v0001/?appid=&key={}&steamid={}".format(appid, user_key, steam_id)

    rval = {

        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {

            "status": "achievements_by_id",
            "result": requests.get(url).json()
        }
    except:
        rval = {

            "status": "error",
            "message": "Player probably doesn't have the app id requested."
        }

    return rval


@app.get("/api/stats_by_id/", tags=["Steam Powered"])
async def stats_by_id():
    url = "http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid={}&key={}&steamid={}".format(default_app_id, user_key, steam_id)

    rval = {

        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {

            "status": "stats_by_id",
            "result": requests.get(url).json()
        }
    except:
        rval = {

            "status": "error",
            "message": "Player probably doesn't have the app id requested."
        }

    return rval


@app.get("/api/stats_by_id/{appid}", tags=["Steam Powered"])
async def stats_by_id(appid, ):
    url = "http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=&key={}&steamid={}".format(appid, user_key, steam_id)

    rval = {

        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {

            "status": "stats_by_id",
            "result": requests.get(url).json()
        }
    except:
        rval = {

            "status": "error",
            "message": "Player probably doesn't have the app id requested."
        }

    return rval


@app.get("/api/games_owned", tags=["Steam Powered"])
async def games_owned():
    url = f"http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key={user_key}&steamid={steam_id}&include_appinfo=true&include_played_free_games=true&format=json"

    rval = {

        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {

            "status": "games_owned",
            "result": requests.get(url).json()
        }
    except:
        rval = {

            "status": "error",
            "message": "things went sideways quick like, but we caught it"
        }

    return rval


@app.get("/api/recently_played", tags=["Steam Powered"])
async def recently_played():
    url = f"https://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key={user_key}&steamid={steam_id}&count=200&format=json"

    rval = {

        "status": "error",
        "message": "error on prefetch"
    }

    try:
        rval = {

            "status": "recently_played",
            "result": requests.get(url).json()
        }
    except:
        rval = {

            "status": "error",
            "message": "things went sideways quick like, but we caught it"
        }

    return rval


@app.get("/api/", tags=["No Catagory"])
async def read_root():
    rval = {
        "status": "online",
        "base_url": "https://{0}".format(baseurl),
        "status_check_url": status_check_url,
        "api_info_url": "https://{0}/api/".format(baseurl),
        "swagger_url": "https://{0}/api/docs".format(baseurl),
        "openapi_url": "https://{0}/api/openapi.json".format(baseurl),
        "uptime_seconds": uptime(),
        "cpu_count": os.cpu_count(),
        "memory": memory(),
    }
    return rval
