# app/config.py

import os
from dotenv import load_dotenv
from pydantic import BaseSettings, Field

load_dotenv()
database_url=os.getenv('DATABASE_URL')


class Settings(BaseSettings):   
    db_url = "{0}".format(database_url)
    #print("db_url = {0}".format(database_url))


settings = Settings()
