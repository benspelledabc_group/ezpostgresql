# app/db.py

import databases
import ormar
import sqlalchemy

from .config import settings

database = databases.Database(settings.db_url)
metadata = sqlalchemy.MetaData()


class BaseMeta(ormar.ModelMeta):
    metadata = metadata
    database = database


class User(ormar.Model):
    class Meta(BaseMeta):
        tablename = "users"

    id: int = ormar.Integer(primary_key=True)
    email: str = ormar.String(max_length=128, unique=True, nullable=False)
    active: bool = ormar.Boolean(default=True, nullable=False)



# ALTER TABLE customers
# ADD COLUMN contact_name VARCHAR;
class Sensor_Posts(ormar.Model):
    class Meta(BaseMeta):
        tablename = "sensor_posts"

    id: int = ormar.Integer(primary_key=True)
    json_data: str = ormar.JSON()
    approved: bool = ormar.Boolean(default=False, nullable=True)
    date: date = ormar.DateTime()



# {
#   "title": "cool title",
#   "body": "cool body that goes on and on.. but not really",
#   "thumbnail": "https://picsum.photos/100/100"
# }

# DO
# $do$
# BEGIN
#     FOR i IN 1..100 LOOP
#         INSERT INTO public.diary_posts(
#             author_id, date, reviewed, active, json_data, image_path, catagory)
#             VALUES (1, clock_timestamp(), False, False,
#                     '{
#                         "title": "Cool Title",
#                         "body": "cool body that goes on and on.. but not really",
#                         "thumbnail": "https://picsum.photos/100/100"
#                     }',
#                     'https://picsum.photos/200/300?random=1',
#                     'Sample_Data'
#                    );
#         PERFORM pg_sleep(.2);
#     END LOOP;
# END
# $do$;


class Diary_Posts(ormar.Model):
    class Meta(BaseMeta):
        tablename = "diary_posts"

    id: int = ormar.Integer(primary_key=True)
    author_id: int = ormar.Integer()
    date: date = ormar.DateTime()
    reviewed: bool = ormar.Boolean(default=False, nullable=False)
    approved: bool = ormar.Boolean(default=False, nullable=True)
    active: bool = ormar.Boolean(default=False, nullable=False)
    image_path: str = ormar.String(max_length=999, nullable=True)
    catagory: str = ormar.String(max_length=30, nullable=False)
    # this seams easy but it might also be nasty later...
    json_data: str = ormar.JSON()




engine = sqlalchemy.create_engine(settings.db_url)
metadata.create_all(engine)
